Parametric model of diffraction gratings

Quick start
-----------

Open `grating2D.pro' with Gmsh.

Additional info
---------------

A documentation is available here: https://arxiv.org/abs/1710.11451

