A demo of non-linear eigenvalue problems in GetDP.

This model computes one selected eigenfrequency of a grating made of a 
frequency-dispersive material. Five different formulations are given,
calling the polynomial and (rational) non-linear solvers of the SLEPc library
thanks to the unified Eig operator.

Quick start
-----------

1- Download a recent ONELAB version from http://onelab.info/
2- Open `NonLinearEVP.pro' with Gmsh

Additional info
---------------

Some documentation is available here: https://arxiv.org/abs/1802.02363
By default, this model outputs the eigenvalue targeted in the convergence test of the above reference, with the "Rational Non-Linear Eigenvalue Problem" (NEP) SLEPc solver.