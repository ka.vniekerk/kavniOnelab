///////////////////////////////
// Author : Guillaume Demesy //
// scattering.geo            //
///////////////////////////////

Include "scattering_data.geo";
In_n =  Sqrt[Fabs[epsr_In_re]];
// Out_n = Sqrt[Fabs[epsr_Out_re]];
  
paramaille_pml = paramaille/1.1;

Out_lc        = lambda_bg/paramaille;
PML_lc        = lambda_bg/paramaille_pml;
In_lc         = lambda0/(paramaille*In_n*refine_scat);
CenterScat_lc = lambda0/(paramaille*In_n);
  
If(flag_cartpml==1)
  dom_x   = r_pml_in*2;
  dom_y   = r_pml_in*2;
  dom_z   = r_pml_in*2;
  PML_bot = pml_size; 
  PML_lat = pml_size; 
  PML_top = pml_size; 
  Point(1)  = {-dom_x/2,-dom_y/2, -dom_z/2, Out_lc};
  Point(2)  = {-dom_x/2, dom_y/2, -dom_z/2, Out_lc};
  Point(3)  = { dom_x/2, dom_y/2, -dom_z/2, Out_lc};
  Point(4)  = { dom_x/2,-dom_y/2, -dom_z/2, Out_lc};
  Point(5)  = {-dom_x/2,-dom_y/2,  dom_z/2, Out_lc};
  Point(6)  = {-dom_x/2, dom_y/2,  dom_z/2, Out_lc};
  Point(7)  = { dom_x/2, dom_y/2,  dom_z/2, Out_lc};
  Point(8)  = { dom_x/2,-dom_y/2,  dom_z/2, Out_lc};
  Point(9)   = {-dom_x/2,-dom_y/2, -dom_z/2-PML_bot, PML_lc};
  Point(10)  = {-dom_x/2, dom_y/2, -dom_z/2-PML_bot, PML_lc};
  Point(11)  = { dom_x/2, dom_y/2, -dom_z/2-PML_bot, PML_lc};
  Point(12)  = { dom_x/2,-dom_y/2, -dom_z/2-PML_bot, PML_lc};
  Point(13)  = {-dom_x/2,-dom_y/2,  dom_z/2+PML_top, PML_lc};
  Point(14)  = {-dom_x/2, dom_y/2,  dom_z/2+PML_top, PML_lc};
  Point(15)  = { dom_x/2, dom_y/2,  dom_z/2+PML_top, PML_lc};
  Point(16)  = { dom_x/2,-dom_y/2,  dom_z/2+PML_top, PML_lc};
  Point(17)  = {-dom_x/2,-dom_y/2-PML_lat, -dom_z/2 , PML_lc};
  Point(18)  = {-dom_x/2, dom_y/2+PML_lat, -dom_z/2 , PML_lc};
  Point(19)  = { dom_x/2, dom_y/2+PML_lat, -dom_z/2 , PML_lc};
  Point(20)  = { dom_x/2,-dom_y/2-PML_lat, -dom_z/2 , PML_lc};
  Point(21)  = {-dom_x/2,-dom_y/2-PML_lat,  dom_z/2 , PML_lc};
  Point(22)  = {-dom_x/2, dom_y/2+PML_lat,  dom_z/2 , PML_lc};
  Point(23)  = { dom_x/2, dom_y/2+PML_lat,  dom_z/2 , PML_lc};
  Point(24)  = { dom_x/2,-dom_y/2-PML_lat,  dom_z/2 , PML_lc};
  Point(25)  = {-dom_x/2-PML_lat,-dom_y/2, -dom_z/2 , PML_lc};
  Point(26)  = {-dom_x/2-PML_lat, dom_y/2, -dom_z/2 , PML_lc};
  Point(27)  = { dom_x/2+PML_lat, dom_y/2, -dom_z/2 , PML_lc};
  Point(28)  = { dom_x/2+PML_lat,-dom_y/2, -dom_z/2 , PML_lc};
  Point(29)  = {-dom_x/2-PML_lat,-dom_y/2,  dom_z/2 , PML_lc};
  Point(30)  = {-dom_x/2-PML_lat, dom_y/2,  dom_z/2 , PML_lc};
  Point(31)  = { dom_x/2+PML_lat, dom_y/2,  dom_z/2 , PML_lc};
  Point(32)  = { dom_x/2+PML_lat,-dom_y/2,  dom_z/2 , PML_lc};
  Point(33)  = {-dom_x/2-PML_lat,-dom_y/2-PML_lat, -dom_z/2-PML_bot, PML_lc};
  Point(34)  = {-dom_x/2-PML_lat, dom_y/2+PML_lat, -dom_z/2-PML_bot, PML_lc};
  Point(35)  = { dom_x/2+PML_lat, dom_y/2+PML_lat, -dom_z/2-PML_bot, PML_lc};
  Point(36)  = { dom_x/2+PML_lat,-dom_y/2-PML_lat, -dom_z/2-PML_bot, PML_lc};
  Point(37)  = {-dom_x/2-PML_lat,-dom_y/2-PML_lat,  dom_z/2+PML_top, PML_lc};
  Point(38)  = {-dom_x/2-PML_lat, dom_y/2+PML_lat,  dom_z/2+PML_top, PML_lc};
  Point(39)  = { dom_x/2+PML_lat, dom_y/2+PML_lat,  dom_z/2+PML_top, PML_lc};
  Point(40)  = { dom_x/2+PML_lat,-dom_y/2-PML_lat,  dom_z/2+PML_top, PML_lc};
  Point(41)  = {-dom_x/2,-dom_y/2-PML_lat, -dom_z/2-PML_bot, PML_lc};
  Point(42)  = {-dom_x/2, dom_y/2+PML_lat, -dom_z/2-PML_bot, PML_lc};
  Point(43)  = { dom_x/2,-dom_y/2-PML_lat, -dom_z/2-PML_top, PML_lc};
  Point(44)  = { dom_x/2, dom_y/2+PML_lat, -dom_z/2-PML_top, PML_lc};
  Point(45)  = {-dom_x/2,-dom_y/2-PML_lat,  dom_z/2+PML_bot, PML_lc};
  Point(46)  = {-dom_x/2, dom_y/2+PML_lat,  dom_z/2+PML_bot, PML_lc};
  Point(47)  = { dom_x/2,-dom_y/2-PML_lat,  dom_z/2+PML_top, PML_lc};
  Point(48)  = { dom_x/2, dom_y/2+PML_lat,  dom_z/2+PML_top, PML_lc};
  Point(49)  = {-dom_x/2-PML_lat, -dom_y/2, -dom_z/2-PML_bot, PML_lc};
  Point(50)  = { dom_x/2+PML_lat, -dom_y/2, -dom_z/2-PML_bot, PML_lc};
  Point(51)  = {-dom_x/2-PML_lat, -dom_y/2,  dom_z/2+PML_bot, PML_lc};
  Point(52)  = { dom_x/2+PML_lat, -dom_y/2,  dom_z/2+PML_bot, PML_lc};
  Point(53)  = {-dom_x/2-PML_lat,  dom_y/2, -dom_z/2-PML_bot, PML_lc};
  Point(54)  = { dom_x/2+PML_lat,  dom_y/2, -dom_z/2-PML_bot, PML_lc};
  Point(55)  = {-dom_x/2-PML_lat,  dom_y/2,  dom_z/2+PML_bot, PML_lc};
  Point(56)  = { dom_x/2+PML_lat,  dom_y/2,  dom_z/2+PML_bot, PML_lc};
  Point(57)  = {-dom_x/2-PML_lat, -dom_y/2-PML_lat, -dom_z/2, PML_lc};
  Point(58)  = { dom_x/2+PML_lat, -dom_y/2-PML_lat, -dom_z/2, PML_lc};
  Point(59)  = {-dom_x/2-PML_lat,  dom_y/2+PML_lat, -dom_z/2, PML_lc};
  Point(60)  = { dom_x/2+PML_lat,  dom_y/2+PML_lat, -dom_z/2, PML_lc};
  Point(61)  = {-dom_x/2-PML_lat, -dom_y/2-PML_lat,  dom_z/2, PML_lc};
  Point(62)  = { dom_x/2+PML_lat, -dom_y/2-PML_lat,  dom_z/2, PML_lc};
  Point(63)  = {-dom_x/2-PML_lat,  dom_y/2+PML_lat,  dom_z/2, PML_lc};
  Point(64)  = { dom_x/2+PML_lat,  dom_y/2+PML_lat,  dom_z/2, PML_lc};
  Point(73)  = { 0,0,0, CenterScat_lc};
  
  Line(1) = {33, 49};
  Line(2) = {49, 53};
  Line(3) = {53, 34};
  Line(4) = {34, 42};
  Line(5) = {42, 44};
  Line(6) = {44, 35};
  Line(7) = {35, 54};
  Line(8) = {54, 11};
  Line(9) = {11, 10};
  Line(10) = {10, 53};
  Line(11) = {49, 9};
  Line(12) = {9, 12};
  Line(13) = {12, 50};
  Line(14) = {50, 54};
  Line(15) = {50, 36};
  Line(16) = {36, 43};
  Line(17) = {43, 41};
  Line(18) = {41, 33};
  Line(19) = {41, 9};
  Line(20) = {9, 10};
  Line(21) = {10, 42};
  Line(22) = {43, 12};
  Line(23) = {12, 11};
  Line(24) = {11, 44};
  Line(25) = {57, 25};
  Line(26) = {25, 26};
  Line(27) = {26, 59};
  Line(28) = {59, 18};
  Line(29) = {18, 19};
  Line(30) = {19, 60};
  Line(31) = {60, 27};
  Line(32) = {27, 28};
  Line(33) = {28, 58};
  Line(34) = {58, 20};
  Line(35) = {20, 17};
  Line(36) = {17, 57};
  Line(37) = {25, 1};
  Line(38) = {1, 4};
  Line(39) = {4, 28};
  Line(40) = {27, 3};
  Line(41) = {3, 2};
  Line(42) = {2, 26};
  Line(43) = {17, 1};
  Line(44) = {1, 2};
  Line(45) = {2, 18};
  Line(46) = {19, 3};
  Line(47) = {3, 4};
  Line(48) = {4, 20};
  Line(49) = {61, 21};
  Line(50) = {21, 24};
  Line(51) = {24, 62};
  Line(52) = {62, 32};
  Line(53) = {32, 31};
  Line(54) = {31, 64};
  Line(55) = {64, 23};
  Line(56) = {23, 22};
  Line(57) = {22, 63};
  Line(58) = {63, 30};
  Line(59) = {30, 29};
  Line(60) = {29, 61};
  Line(61) = {21, 5};
  Line(62) = {5, 6};
  Line(63) = {6, 22};
  Line(64) = {23, 7};
  Line(65) = {7, 8};
  Line(66) = {8, 24};
  Line(67) = {32, 8};
  Line(68) = {8, 5};
  Line(69) = {5, 29};
  Line(70) = {30, 6};
  Line(71) = {6, 7};
  Line(72) = {7, 31};
  Line(73) = {37, 45};
  Line(74) = {45, 47};
  Line(75) = {47, 40};
  Line(76) = {40, 52};
  Line(77) = {52, 56};
  Line(78) = {56, 39};
  Line(79) = {39, 48};
  Line(80) = {48, 46};
  Line(81) = {46, 38};
  Line(82) = {38, 55};
  Line(83) = {55, 51};
  Line(84) = {51, 37};
  Line(85) = {45, 13};
  Line(86) = {13, 14};
  Line(87) = {14, 46};
  Line(88) = {55, 14};
  Line(89) = {14, 15};
  Line(90) = {15, 56};
  Line(91) = {52, 16};
  Line(92) = {16, 13};
  Line(93) = {13, 51};
  Line(94) = {48, 15};
  Line(95) = {15, 16};
  Line(96) = {16, 47};
  Line(97) = {37, 61};
  Line(98) = {61, 57};
  Line(99) = {57, 33};
  Line(100) = {45, 21};
  Line(101) = {21, 17};
  Line(102) = {17, 41};
  Line(103) = {47, 24};
  Line(104) = {24, 20};
  Line(105) = {20, 43};
  Line(106) = {40, 62};
  Line(107) = {62, 58};
  Line(108) = {58, 36};
  Line(109) = {52, 32};
  Line(110) = {32, 28};
  Line(111) = {28, 50};
  Line(112) = {56, 31};
  Line(113) = {31, 27};
  Line(114) = {27, 54};
  Line(115) = {39, 64};
  Line(116) = {64, 60};
  Line(117) = {60, 35};
  Line(118) = {48, 23};
  Line(119) = {23, 19};
  Line(120) = {19, 44};
  Line(121) = {46, 22};
  Line(122) = {22, 18};
  Line(123) = {18, 42};
  Line(124) = {38, 63};
  Line(125) = {63, 59};
  Line(126) = {59, 34};
  Line(127) = {55, 30};
  Line(128) = {30, 26};
  Line(129) = {26, 53};
  Line(130) = {51, 29};
  Line(131) = {29, 25};
  Line(132) = {25, 49};
  Line(133) = {14, 6};
  Line(134) = {6, 2};
  Line(135) = {2, 10};
  Line(136) = {15, 7};
  Line(137) = {7, 3};
  Line(138) = {3, 11};
  Line(139) = {16, 8};
  Line(140) = {8, 4};
  Line(141) = {4, 12};
  Line(142) = {13, 5};
  Line(143) = {5, 1};
  Line(144) = {1, 9};

  Line Loop(173) = {80, -87, 89, -94}; Plane Surface(174) = {173};
  Line Loop(175) = {94, 90, 78, 79}; Plane Surface(176) = {175};
  Line Loop(177) = {77, -90, 95, -91}; Plane Surface(178) = {177};
  Line Loop(179) = {95, 92, 86, 89}; Plane Surface(180) = {179};
  Line Loop(181) = {91, 96, 75, 76}; Plane Surface(182) = {181};
  Line Loop(183) = {92, -85, 74, -96}; Plane Surface(184) = {183};
  Line Loop(185) = {82, 88, 87, 81}; Plane Surface(186) = {185};
  Line Loop(187) = {86, -88, 83, -93}; Plane Surface(188) = {187};
  Line Loop(189) = {93, 84, 73, 85}; Plane Surface(190) = {189};
  Line Loop(191) = {58, 70, 63, 57}; Plane Surface(192) = {191};
  Line Loop(193) = {63, -56, 64, -71}; Plane Surface(194) = {193};
  Line Loop(195) = {64, 72, 54, 55}; Plane Surface(196) = {195};
  Line Loop(197) = {72, -53, 67, -65}; Plane Surface(198) = {197};
  Line Loop(199) = {67, 66, 51, 52}; Plane Surface(200) = {199};
  Line Loop(201) = {71, 65, 68, 62}; Plane Surface(202) = {201};
  Line Loop(203) = {68, -61, 50, -66}; Plane Surface(204) = {203};
  Line Loop(205) = {59, -69, 62, -70}; Plane Surface(206) = {205};
  Line Loop(207) = {69, 60, 49, 61}; Plane Surface(208) = {207};
  Line Loop(209) = {28, -45, 42, 27}; Plane Surface(210) = {209};
  Line Loop(211) = {29, 46, 41, 45}; Plane Surface(212) = {211};
  Line Loop(213) = {30, 31, 40, -46}; Plane Surface(214) = {213};
  Line Loop(215) = {40, 47, 39, -32}; Plane Surface(216) = {215};
  Line Loop(217) = {41, -44, 38, -47}; Plane Surface(218) = {217};
  Line Loop(219) = {39, 33, 34, -48}; Plane Surface(220) = {219};
  Line Loop(221) = {38, 48, 35, 43}; Plane Surface(222) = {221};
  Line Loop(223) = {26, -42, -44, -37}; Plane Surface(224) = {223};
  Line Loop(225) = {37, -43, 36, 25}; Plane Surface(226) = {225};
  Line Loop(227) = {3, 4, -21, 10}; Plane Surface(228) = {227};
  Line Loop(229) = {5, -24, 9, 21}; Plane Surface(230) = {229};
  Line Loop(231) = {6, 7, 8, 24}; Plane Surface(232) = {231};
  Line Loop(233) = {14, 8, -23, 13}; Plane Surface(234) = {233};
  Line Loop(235) = {9, -20, 12, 23}; Plane Surface(236) = {235};
  Line Loop(237) = {2, -10, -20, -11}; Plane Surface(238) = {237};
  Line Loop(239) = {13, 15, 16, 22}; Plane Surface(240) = {239};
  Line Loop(241) = {12, -22, 17, 19}; Plane Surface(242) = {241};
  Line Loop(243) = {19, -11, -1, -18}; Plane Surface(244) = {243};
  Line Loop(245) = {127, 70, -133, -88}; Plane Surface(246) = {245};
  Line Loop(247) = {133, 71, -136, -89}; Plane Surface(248) = {247};
  Line Loop(249) = {90, 112, -72, -136}; Plane Surface(250) = {249};
  Line Loop(251) = {128, -42, -134, -70}; Plane Surface(252) = {251};
  Line Loop(253) = {41, -134, 71, 137}; Plane Surface(254) = {253};
  Line Loop(255) = {137, -40, -113, -72}; Plane Surface(256) = {255};
  Line Loop(257) = {129, -10, -135, 42}; Plane Surface(258) = {257};
  Line Loop(259) = {135, -9, -138, 41}; Plane Surface(260) = {259};
  Line Loop(261) = {138, -8, -114, 40}; Plane Surface(262) = {261};
  Line Loop(263) = {130, -69, -142, 93}; Plane Surface(264) = {263};
  Line Loop(265) = {92, 142, -68, -139}; Plane Surface(266) = {265};
  Line Loop(267) = {139, -67, -109, 91}; Plane Surface(268) = {267};
  Line Loop(269) = {131, 37, -143, 69}; Plane Surface(270) = {269};
  Line Loop(271) = {143, 38, -140, 68}; Plane Surface(272) = {271};
  Line Loop(273) = {140, 39, -110, 67}; Plane Surface(274) = {273};
  Line Loop(275) = {132, 11, -144, -37}; Plane Surface(276) = {275};
  Line Loop(277) = {12, -141, -38, 144}; Plane Surface(278) = {277};
  Line Loop(279) = {141, 13, -111, -39}; Plane Surface(280) = {279};
  Line Loop(281) = {99, -18, -102, 36}; Plane Surface(282) = {281};
  Line Loop(283) = {102, -17, -105, 35}; Plane Surface(284) = {283};
  Line Loop(285) = {34, 105, -16, -108}; Plane Surface(286) = {285};
  Line Loop(287) = {34, -104, 51, 107}; Plane Surface(288) = {287};
  Line Loop(289) = {50, 104, 35, -101}; Plane Surface(290) = {289};
  Line Loop(291) = {36, -98, 49, 101}; Plane Surface(292) = {291};
  Line Loop(293) = {97, 49, -100, -73}; Plane Surface(294) = {293};
  Line Loop(295) = {74, 103, -50, -100}; Plane Surface(296) = {295};
  Line Loop(297) = {103, 51, -106, -75}; Plane Surface(298) = {297};
  Line Loop(299) = {126, 4, -123, -28}; Plane Surface(300) = {299};
  Line Loop(301) = {125, 28, -122, 57}; Plane Surface(302) = {301};
  Line Loop(303) = {122, 29, -119, 56}; Plane Surface(304) = {303};
  Line Loop(305) = {119, 30, -116, 55}; Plane Surface(306) = {305};
  Line Loop(307) = {55, -118, -79, 115}; Plane Surface(308) = {307};
  Line Loop(309) = {80, 121, -56, -118}; Plane Surface(310) = {309};
  Line Loop(311) = {81, 124, -57, -121}; Plane Surface(312) = {311};
  Line Loop(313) = {123, 5, -120, -29}; Plane Surface(314) = {313};
  Line Loop(315) = {120, 6, -117, -30}; Plane Surface(316) = {315};
  Line Loop(317) = {121, -63, -133, 87}; Plane Surface(318) = {317};
  Line Loop(319) = {133, -62, -142, 86}; Plane Surface(320) = {319};
  Line Loop(321) = {142, -61, -100, 85}; Plane Surface(322) = {321};
  Line Loop(323) = {122, -45, -134, 63}; Plane Surface(324) = {323};
  Line Loop(325) = {134, -44, -143, 62}; Plane Surface(326) = {325};
  Line Loop(327) = {143, -43, -101, 61}; Plane Surface(328) = {327};
  Line Loop(329) = {123, -21, -135, 45}; Plane Surface(330) = {329};
  Line Loop(331) = {135, -20, -144, 44}; Plane Surface(332) = {331};
  Line Loop(333) = {144, -19, -102, 43}; Plane Surface(334) = {333};
  Line Loop(335) = {126, -3, -129, 27}; Plane Surface(336) = {335};
  Line Loop(337) = {129, -2, -132, 26}; Plane Surface(338) = {337};
  Line Loop(339) = {132, -1, -99, 25}; Plane Surface(340) = {339};
  Line Loop(341) = {125, -27, -128, -58}; Plane Surface(342) = {341};
  Line Loop(343) = {128, -26, -131, -59}; Plane Surface(344) = {343};
  Line Loop(345) = {131, -25, -98, -60}; Plane Surface(346) = {345};
  Line Loop(347) = {97, -60, -130, 84}; Plane Surface(348) = {347};
  Line Loop(349) = {83, 130, -59, -127}; Plane Surface(350) = {349};
  Line Loop(351) = {58, -127, -82, 124}; Plane Surface(352) = {351};
  Line Loop(353) = {96, 103, -66, -139}; Plane Surface(354) = {353};
  Line Loop(355) = {139, -65, -136, 95}; Plane Surface(356) = {355};
  Line Loop(357) = {136, -64, -118, 94}; Plane Surface(358) = {357};
  Line Loop(359) = {66, 104, -48, -140}; Plane Surface(360) = {359};
  Line Loop(361) = {140, -47, -137, 65}; Plane Surface(362) = {361};
  Line Loop(363) = {137, -46, -119, 64}; Plane Surface(364) = {363};
  Line Loop(365) = {105, 22, -141, 48}; Plane Surface(366) = {365};
  Line Loop(367) = {141, 23, -138, 47}; Plane Surface(368) = {367};
  Line Loop(369) = {138, 24, -120, 46}; Plane Surface(370) = {369};
  Line Loop(371) = {108, -15, -111, 33}; Plane Surface(372) = {371};
  Line Loop(373) = {111, 14, -114, 32}; Plane Surface(374) = {373};
  Line Loop(375) = {114, -7, -117, 31}; Plane Surface(376) = {375};
  Line Loop(377) = {107, -33, -110, -52}; Plane Surface(378) = {377};
  Line Loop(379) = {110, -32, -113, -53}; Plane Surface(380) = {379};
  Line Loop(381) = {113, -31, -116, -54}; Plane Surface(382) = {381};
  Line Loop(383) = {106, 52, -109, -76}; Plane Surface(384) = {383};
  Line Loop(385) = {109, 53, -112, -77}; Plane Surface(386) = {385};
  Line Loop(387) = {112, 54, -115, -78}; Plane Surface(388) = {387};
  
  Surface Loop(393) = {182, 298, 384, 354, 200, 268}; Volume(394) = {393};
  Surface Loop(395) = {184, 296, 354, 266, 204, 322}; Volume(396) = {395};
  Surface Loop(397) = {190, 348, 294, 264, 208, 322}; Volume(398) = {397};
  Surface Loop(399) = {188, 350, 264, 246, 320, 206}; Volume(400) = {399};
  Surface Loop(401) = {186, 352, 312, 192, 318, 246}; Volume(402) = {401};
  Surface Loop(403) = {180, 248, 266, 202, 356, 320}; Volume(404) = {403};
  Surface Loop(405) = {310, 174, 318, 248, 194, 358}; Volume(406) = {405};
  Surface Loop(407) = {176, 388, 308, 358, 250, 196}; Volume(408) = {407};
  Surface Loop(409) = {178, 386, 198, 356, 268, 250}; Volume(410) = {409};
  Surface Loop(411) = {378, 288, 200, 274, 360, 220}; Volume(412) = {411};
  Surface Loop(413) = {216, 380, 198, 274, 256, 362}; Volume(414) = {413};
  Surface Loop(415) = {306, 382, 214, 364, 196, 256}; Volume(416) = {415};
  Surface Loop(417) = {304, 194, 364, 212, 254, 324}; Volume(418) = {417};
  Surface Loop(419) = {302, 342, 192, 324, 252, 210}; Volume(420) = {419};
  Surface Loop(421) = {344, 206, 252, 224, 270, 326}; Volume(422) = {421};
  Surface Loop(423) = {292, 346, 328, 208, 270, 226}; Volume(424) = {423};
  Surface Loop(425) = {290, 360, 204, 272, 328, 222}; Volume(426) = {425};
  Surface Loop(427) = {240, 372, 286, 220, 366, 280}; Volume(428) = {427};
  Surface Loop(429) = {234, 374, 216, 368, 262, 280}; Volume(430) = {429};
  Surface Loop(431) = {376, 232, 316, 370, 214, 262}; Volume(432) = {431};
  Surface Loop(433) = {314, 230, 260, 212, 330, 370}; Volume(434) = {433};
  Surface Loop(435) = {228, 336, 300, 210, 258, 330}; Volume(436) = {435};
  Surface Loop(437) = {236, 332, 368, 278, 260, 218}; Volume(438) = {437};
  Surface Loop(439) = {284, 242, 366, 334, 222, 278}; Volume(440) = {439};
  Surface Loop(441) = {282, 340, 244, 334, 226, 276}; Volume(442) = {441};
  Surface Loop(443) = {238, 338, 258, 276, 332, 224}; Volume(444) = {443};

  pt_sc=newp;
  // Printf("newp scatterer=%g",pt_sc);
  l_sc =newl;
  ll_sc =newll;
  s_sc =news;
  
  If(flag_shape==ELL)
    Point(pt_sc+20) = { 0, 0, ell_rz, In_lc}; Point(pt_sc+21) = { 0, 0,-ell_rz, In_lc};
    Point(pt_sc+22) = { 0, ell_ry, 0, In_lc}; Point(pt_sc+23) = { 0,-ell_ry, 0, In_lc};
    Point(pt_sc+25) = { ell_rx, 0, 0, In_lc}; Point(pt_sc+26) = {-ell_rx, 0, 0, In_lc};
    Ellipse(l_sc+145) = {pt_sc+22, 73, pt_sc+26, pt_sc+26} Plane{0,0,1};
    Ellipse(l_sc+146) = {pt_sc+26, 73, pt_sc+23, pt_sc+23} Plane{0,0,1};
    Ellipse(l_sc+147) = {pt_sc+23, 73, pt_sc+25, pt_sc+25} Plane{0,0,1};
    Ellipse(l_sc+148) = {pt_sc+25, 73, pt_sc+22, pt_sc+22} Plane{0,0,1};
    Ellipse(l_sc+149) = {pt_sc+21, 73, pt_sc+26, pt_sc+26} Plane{0,0,1};
    Ellipse(l_sc+150) = {pt_sc+26, 73, pt_sc+20, pt_sc+20} Plane{0,0,1};
    Ellipse(l_sc+151) = {pt_sc+20, 73, pt_sc+25, pt_sc+25} Plane{0,0,1};
    Ellipse(l_sc+152) = {pt_sc+25, 73, pt_sc+21, pt_sc+21} Plane{0,0,1};
    Ellipse(l_sc+153) = {pt_sc+23, 73, pt_sc+20, pt_sc+20} Plane{0,0,1};
    Ellipse(l_sc+154) = {pt_sc+20, 73, pt_sc+22, pt_sc+22} Plane{0,0,1};
    Ellipse(l_sc+155) = {pt_sc+22, 73, pt_sc+21, pt_sc+21} Plane{0,0,1};
    Ellipse(l_sc+156) = {pt_sc+21, 73, pt_sc+23, pt_sc+23} Plane{0,0,1};
    Line Loop(ll_sc+157) = {l_sc+151, -147-l_sc,  153+l_sc};  Surface(158+s_sc) = {157+ll_sc};
    Line Loop(ll_sc+159) = {l_sc+151,  148+l_sc, -154-l_sc};  Surface(160+s_sc) = {159+ll_sc};
    Line Loop(ll_sc+161) = {l_sc+145,  150+l_sc,  154+l_sc};  Surface(162+s_sc) = {161+ll_sc};
    Line Loop(ll_sc+163) = {l_sc+150, -153-l_sc, -146-l_sc};  Surface(164+s_sc) = {163+ll_sc};
    Line Loop(ll_sc+165) = {l_sc+156,  147+l_sc,  152+l_sc};  Surface(166+s_sc) = {165+ll_sc};
    Line Loop(ll_sc+167) = {l_sc+152, -155-l_sc, -148-l_sc};  Surface(168+s_sc) = {167+ll_sc};
    Line Loop(ll_sc+169) = {l_sc+155,  149+l_sc, -145-l_sc};  Surface(170+s_sc) = {169+ll_sc};
    Line Loop(ll_sc+171) = {l_sc+149,  146+l_sc, -156-l_sc};  Surface(172+s_sc) = {171+ll_sc};
    Surface Loop(1072) = {160+s_sc, 162+s_sc, 164+s_sc, 158+s_sc, 166+s_sc, 170+s_sc, 172+s_sc, 168+s_sc};
    Volume(1092) = {1072};
  EndIf  
  If(flag_shape==PARALL)
    Point(pt_sc+20) = {-par_ax/2,-par_ay/2,-par_az/2,In_lc};
    Point(pt_sc+21) = { par_ax/2,-par_ay/2,-par_az/2,In_lc};
    Point(pt_sc+22) = { par_ax/2, par_ay/2,-par_az/2,In_lc};
    Point(pt_sc+23) = {-par_ax/2, par_ay/2,-par_az/2,In_lc};
    Point(pt_sc+24) = {-par_ax/2,-par_ay/2, par_az/2,In_lc};
    Point(pt_sc+25) = { par_ax/2,-par_ay/2, par_az/2,In_lc};
    Point(pt_sc+26) = { par_ax/2, par_ay/2, par_az/2,In_lc};
    Point(pt_sc+27) = {-par_ax/2, par_ay/2, par_az/2,In_lc};
    Line(l_sc+1) =  {pt_sc+20, pt_sc+24};
    Line(l_sc+2) =  {pt_sc+24, pt_sc+25};
    Line(l_sc+3) =  {pt_sc+25, pt_sc+21};
    Line(l_sc+4) =  {pt_sc+21, pt_sc+20};
    Line(l_sc+5) =  {pt_sc+23, pt_sc+27};
    Line(l_sc+6) =  {pt_sc+27, pt_sc+26};
    Line(l_sc+7) =  {pt_sc+26, pt_sc+22};
    Line(l_sc+8) =  {pt_sc+22, pt_sc+23};
    Line(l_sc+9) =  {pt_sc+23, pt_sc+20};
    Line(l_sc+10) = {pt_sc+22, pt_sc+21};
    Line(l_sc+11) = {pt_sc+26, pt_sc+25};
    Line(l_sc+12) = {pt_sc+27, pt_sc+24};
    Line Loop(ll_sc+13) = {l_sc+2, -11-l_sc, -6-l_sc, l_sc+12};Plane Surface(s_sc+14) = {ll_sc+13};
    Line Loop(ll_sc+15) = {l_sc+1, -12-l_sc, -5-l_sc, l_sc+9};Plane Surface(s_sc+16) = {ll_sc+15};
    Line Loop(ll_sc+17) = {l_sc+3, -10-l_sc, -7-l_sc, l_sc+11};Plane Surface(s_sc+18) = {ll_sc+17};
    Line Loop(ll_sc+19) = {l_sc+8,   5+l_sc,  6+l_sc, l_sc+7};Plane Surface(s_sc+20) = {ll_sc+19};
    Line Loop(ll_sc+21) = {l_sc+4,  -9-l_sc, -8-l_sc, l_sc+10};Plane Surface(s_sc+22) = {ll_sc+21};
    Line Loop(ll_sc+23) = {l_sc+1,   2+l_sc,  3+l_sc, l_sc+4};Plane Surface(s_sc+24) = {ll_sc+23};
    Surface Loop(1072)  = {s_sc+16, 24+s_sc, 14+s_sc, 18+s_sc, 22+s_sc, 20+s_sc};
    Volume(1092) = {1072};
  EndIf
  If(flag_shape==CYL)
    Point(pt_sc+20) = { 0, cyl_ry, -cyl_h/2, In_lc}; 
    Point(pt_sc+21) = { 0,-cyl_ry, -cyl_h/2, In_lc};
    Point(pt_sc+22) = { cyl_rx, 0, -cyl_h/2, In_lc}; 
    Point(pt_sc+23) = {-cyl_rx, 0, -cyl_h/2, In_lc};
    Point(pt_sc+24) = {0, 0, -cyl_h/2, In_lc};
    Point(pt_sc+25) = { 0, cyl_ry,  cyl_h/2, In_lc}; 
    Point(pt_sc+26) = { 0,-cyl_ry,  cyl_h/2, In_lc};
    Point(pt_sc+27) = { cyl_rx, 0,  cyl_h/2, In_lc}; 
    Point(pt_sc+28) = {-cyl_rx, 0,  cyl_h/2, In_lc};
    Point(pt_sc+29) = {0, 0,  cyl_h/2, In_lc};
    Ellipse(l_sc+1) = {pt_sc+23, pt_sc+24, pt_sc+21, pt_sc+21};
    Ellipse(l_sc+2) = {pt_sc+21, pt_sc+24, pt_sc+22, pt_sc+22};
    Ellipse(l_sc+3) = {pt_sc+22, pt_sc+24, pt_sc+20, pt_sc+20};
    Ellipse(l_sc+4) = {pt_sc+20, pt_sc+24, pt_sc+24, pt_sc+23};
    Ellipse(l_sc+5) = {pt_sc+28, pt_sc+29, pt_sc+26, pt_sc+26};
    Ellipse(l_sc+6) = {pt_sc+26, pt_sc+29, pt_sc+27, pt_sc+27};
    Ellipse(l_sc+7) = {pt_sc+27, pt_sc+29, pt_sc+25, pt_sc+25};
    Ellipse(l_sc+8) = {pt_sc+25, pt_sc+29, pt_sc+28, pt_sc+28};
    Line(l_sc+9)  = {pt_sc+26, pt_sc+21};
    Line(l_sc+10) = {pt_sc+28, pt_sc+23};
    Line(l_sc+11) = {pt_sc+25, pt_sc+20};
    Line(l_sc+12) = {pt_sc+27, pt_sc+22};
    Line Loop(ll_sc+13) = {l_sc+8,  5 +l_sc ,   6+l_sc,   7+l_sc};Plane Surface(s_sc+14) = {ll_sc+13};
    Line Loop(ll_sc+15) = {l_sc+4,  1 +l_sc ,   2+l_sc,   3+l_sc};Plane Surface(s_sc+16) = {ll_sc+15};
    Line Loop(ll_sc+17) = {l_sc+8,  10+l_sc ,  -4-l_sc, -11-l_sc};Surface(s_sc+18)       = {ll_sc+17};
    Line Loop(ll_sc+19) = {l_sc+5,  9 +l_sc ,  -1-l_sc, -10-l_sc};Surface(s_sc+20)       = {ll_sc+19};
    Line Loop(ll_sc+21) = {l_sc+9,  2 +l_sc , -12-l_sc,  -6-l_sc};Surface(s_sc+22)       = {ll_sc+21};
    Line Loop(ll_sc+23) = {l_sc+3, -11-l_sc ,  -7-l_sc,  12+l_sc};Surface(s_sc+24)       = {ll_sc+23};
    Surface Loop(1072) = {s_sc+14, s_sc+18, s_sc+20, s_sc+22, s_sc+16, s_sc+24};
    Volume(1092) = {1072};
  EndIf
  If(flag_shape==CONE)
    Point(pt_sc+20) = { 0, cone_ry, -cone_h/3, In_lc}; 
    Point(pt_sc+21) = { 0,-cone_ry, -cone_h/3, In_lc};
    Point(pt_sc+22) = { cone_rx, 0, -cone_h/3, In_lc}; 
    Point(pt_sc+23) = {-cone_rx, 0, -cone_h/3, In_lc};
    Point(pt_sc+24) = {0, 0, -cone_h/3, In_lc};
    Point(pt_sc+26) = {0, 0, 2*cone_h/3, In_lc};
    Ellipse(l_sc+1) = {pt_sc+23, pt_sc+24, pt_sc+21, pt_sc+21};
    Ellipse(l_sc+2) = {pt_sc+21, pt_sc+24, pt_sc+22, pt_sc+22};
    Ellipse(l_sc+3) = {pt_sc+22, pt_sc+24, pt_sc+20, pt_sc+20};
    Ellipse(l_sc+4) = {pt_sc+20, pt_sc+24, pt_sc+24, pt_sc+23};
    Line(l_sc+5)={pt_sc+20,pt_sc+26};Line(l_sc+6)={pt_sc+21,pt_sc+26};
    Line(l_sc+7)={pt_sc+22,pt_sc+26};Line(l_sc+8)={pt_sc+23,pt_sc+26};
    Line Loop(ll_sc+9) = {l_sc+1, l_sc+2, l_sc+3, l_sc+4};Plane Surface(s_sc+10) = {ll_sc+9};
    Line Loop(ll_sc+11) = {l_sc+6, -8-l_sc, 1+l_sc};Surface(s_sc+12) = {ll_sc+11};
    Line Loop(ll_sc+13) = {l_sc+8, -5-l_sc, 4+l_sc};Surface(s_sc+14) = {ll_sc+13};
    Line Loop(ll_sc+15) = {l_sc+5, -7-l_sc, 3+l_sc};Surface(s_sc+16) = {ll_sc+15};
    Line Loop(ll_sc+17) = {l_sc+7, -6-l_sc, 2+l_sc};Surface(s_sc+18) = {ll_sc+17};
    Surface Loop(1072) = {s_sc+12, s_sc+18, s_sc+16, s_sc+14, s_sc+10};
    Volume(1092) = {1072};
  EndIf
  If (flag_shape==TOR)
    Point(pt_sc+20) = { 0, tor_r1+tor_r2x+tor_r2x ,  0, In_lc};
    Point(pt_sc+21) = { 0, tor_r1+tor_r2x-tor_r2x ,  0, In_lc};
    Point(pt_sc+22) = { 0, tor_r1+tor_r2x ,  tor_r2z, In_lc};
    Point(pt_sc+23) = { 0, tor_r1+tor_r2x , -tor_r2z, In_lc};
    Point(pt_sc+24) = { 0, tor_r1+tor_r2x, 0, In_lc};
    Ellipse(l_sc+1) = {pt_sc+21, pt_sc+24, pt_sc+22, pt_sc+22};
    Ellipse(l_sc+2) = {pt_sc+22, pt_sc+24, pt_sc+20, pt_sc+20};
    Ellipse(l_sc+3) = {pt_sc+20, pt_sc+24, pt_sc+23, pt_sc+23};
    Ellipse(l_sc+4) = {pt_sc+23, pt_sc+24, pt_sc+21, pt_sc+21};
    Line Loop(ll_sc+5) = {l_sc+2, l_sc+3, l_sc+4, l_sc+1};Plane Surface(s_sc+6) = {ll_sc+5};
    Extrude {{0, 0, 1}, {0, 0, 0}, deg2rad*tor_angle/2} {Surface{s_sc+6};}
    Extrude {{0, 0, 1}, {0, 0, 0},-deg2rad*tor_angle/2} {Surface{s_sc+6};}
    Surface Loop(1072) = {472, 460, 464, 468, 473, 494, 482, 486, 490, 495};
  EndIf
  inerpml_ll=newll;
  Surface Loop(inerpml_ll) = {326, 272, 218, 362, 202, 254};  
  Volume(1093) = {inerpml_ll,1072};

  Physical Point(2000) = {1}; // PrintPoint
  Physical Volume(1000) = {402, 398, 394, 408, 442, 428, 432, 436}; // PML XYZ
  Physical Volume(1001) = {400, 410, 430, 444}; // PML XZ
  Physical Volume(1002) = {406, 396, 434, 440}; // PML YZ
  Physical Volume(1003) = {412, 416, 420, 424}; // PML XY
  Physical Volume(1004) = {404, 438}; // PML Z
  Physical Volume(1005) = {426, 418}; // PML Y
  Physical Volume(1006) = {414, 422}; // PML X
  Physical Volume(1007) = {1093}; // Out
  If (flag_shape==TOR)
    Physical Volume(1008) = {445,446}; // Scatterer
  Else
    Physical Volume(1008) = {1092}; // Scatterer
  EndIf
  
  // Physical Volume(1007) = {392}; // Out
  // Physical Volume(1008) = {1039,1041,1043,1045,1047,1049,1051,1053}; // Scatterer
EndIf
If(flag_cartpml==0)
  Point(1)  = { r_pml_in, 0, 0 , Out_lc};
  Point(2)  = {-r_pml_in, 0, 0 , Out_lc};
  Point(3)  = {0,  r_pml_in, 0 , Out_lc};
  Point(4)  = {0, -r_pml_in, 0 , Out_lc};
  Point(5)  = {0, 0,  r_pml_in , Out_lc};
  Point(6)  = {0, 0, -r_pml_in , Out_lc};
  Point(7)  = { r_pml_out, 0, 0, PML_lc};
  Point(8)  = {-r_pml_out, 0, 0, PML_lc};
  Point(9)  = {0,  r_pml_out, 0, PML_lc};
  Point(10) = {0, -r_pml_out, 0, PML_lc};
  Point(11) = {0, 0,  r_pml_out, PML_lc};
  Point(12) = {0, 0, -r_pml_out, PML_lc};
  Point(13) = {  0, 0, 0, CenterScat_lc};
  If(flag_shape==ELL)
    Point(20) = { 0, 0, ell_rz, In_lc}; Point(21) = { 0, 0,-ell_rz, In_lc};
    Point(22) = { 0, ell_ry, 0, In_lc}; Point(23) = { 0,-ell_ry, 0, In_lc};
    Point(25) = { ell_rx, 0, 0, In_lc}; Point(26) = {-ell_rx, 0, 0, In_lc};
    Ellipse(145) = {22, 13, 26, 26} Plane{0,0,1};
    Ellipse(146) = {26, 13, 23, 23} Plane{0,0,1};
    Ellipse(147) = {23, 13, 25, 25} Plane{0,0,1};
    Ellipse(148) = {25, 13, 22, 22} Plane{0,0,1};
    Ellipse(149) = {21, 13, 26, 26} Plane{0,0,1};
    Ellipse(150) = {26, 13, 20, 20} Plane{0,0,1};
    Ellipse(151) = {20, 13, 25, 25} Plane{0,0,1};
    Ellipse(152) = {25, 13, 21, 21} Plane{0,0,1};
    Ellipse(153) = {23, 13, 20, 20} Plane{0,0,1};
    Ellipse(154) = {20, 13, 22, 22} Plane{0,0,1};
    Ellipse(155) = {22, 13, 21, 21} Plane{0,0,1};
    Ellipse(156) = {21, 13, 23, 23} Plane{0,0,1};
    Line Loop(157) = {151, -147, 153};  Surface(158) = {157};
    Line Loop(159) = {151, 148, -154};  Surface(160) = {159};
    Line Loop(161) = {145, 150, 154};  Surface(162) = {161};
    Line Loop(163) = {150, -153, -146};  Surface(164) = {163};
    Line Loop(165) = {156, 147, 152};  Surface(166) = {165};
    Line Loop(167) = {152, -155, -148};  Surface(168) = {167};
    Line Loop(169) = {155, 149, -145};  Surface(170) = {169};
    Line Loop(171) = {149, 146, -156};  Surface(172) = {171};
    Surface Loop(1072) = {160, 162, 164, 158, 166, 170, 172, 168};
    Volume(1092) = {1072};
  EndIf  
  If(flag_shape==PARALL)
    Point(20) = {-par_ax/2,-par_ay/2,-par_az/2,In_lc};
    Point(21) = { par_ax/2,-par_ay/2,-par_az/2,In_lc};
    Point(22) = { par_ax/2, par_ay/2,-par_az/2,In_lc};
    Point(23) = {-par_ax/2, par_ay/2,-par_az/2,In_lc};
    Point(24) = {-par_ax/2,-par_ay/2, par_az/2,In_lc};
    Point(25) = { par_ax/2,-par_ay/2, par_az/2,In_lc};
    Point(26) = { par_ax/2, par_ay/2, par_az/2,In_lc};
    Point(27) = {-par_ax/2, par_ay/2, par_az/2,In_lc};
    Line(1) = {20, 24};
    Line(2) = {24, 25};
    Line(3) = {25, 21};
    Line(4) = {21, 20};
    Line(5) = {23, 27};
    Line(6) = {27, 26};
    Line(7) = {26, 22};
    Line(8) = {22, 23};
    Line(9) = {23, 20};
    Line(10) = {22, 21};
    Line(11) = {26, 25};
    Line(12) = {27, 24};
    Line Loop(13) = {2, -11, -6, 12};Plane Surface(14) = {13};
    Line Loop(15) = {1, -12, -5, 9};Plane Surface(16) = {15};
    Line Loop(17) = {3, -10, -7, 11};Plane Surface(18) = {17};
    Line Loop(19) = {8, 5, 6, 7};Plane Surface(20) = {19};
    Line Loop(21) = {4, -9, -8, 10};Plane Surface(22) = {21};
    Line Loop(23) = {1, 2, 3, 4};Plane Surface(24) = {23};
    Surface Loop(1072) = {16, 24, 14, 18, 22, 20};
    Volume(1092) = {1072};
  EndIf
  If(flag_shape==CYL)
    Point(20) = { 0, cyl_ry, -cyl_h/2, In_lc}; 
    Point(21) = { 0,-cyl_ry, -cyl_h/2, In_lc};
    Point(22) = { cyl_rx, 0, -cyl_h/2, In_lc}; 
    Point(23) = {-cyl_rx, 0, -cyl_h/2, In_lc};
    Point(24) = {0, 0, -cyl_h/2, In_lc};
    Point(25) = { 0, cyl_ry,  cyl_h/2, In_lc}; 
    Point(26) = { 0,-cyl_ry,  cyl_h/2, In_lc};
    Point(27) = { cyl_rx, 0,  cyl_h/2, In_lc}; 
    Point(28) = {-cyl_rx, 0,  cyl_h/2, In_lc};
    Point(29) = {0, 0,  cyl_h/2, In_lc};
    Ellipse(1) = {23, 24, 21, 21};
    Ellipse(2) = {21, 24, 22, 22};
    Ellipse(3) = {22, 24, 20, 20};
    Ellipse(4) = {20, 24, 24, 23};
    Ellipse(5) = {28, 29, 26, 26};
    Ellipse(6) = {26, 29, 27, 27};
    Ellipse(7) = {27, 29, 25, 25};
    Ellipse(8) = {25, 29, 28, 28};
    Line(9) = {26, 21};
    Line(10) = {28, 23};
    Line(11) = {25, 20};
    Line(12) = {27, 22};
    Line Loop(13) = {8, 5, 6, 7};Plane Surface(14) = {13};
    Line Loop(15) = {4, 1, 2, 3};Plane Surface(16) = {15};
    Line Loop(17) = {8, 10, -4, -11};Surface(18) = {17};
    Line Loop(19) = {5, 9, -1, -10};Surface(20) = {19};
    Line Loop(21) = {9, 2, -12, -6};Surface(22) = {21};
    Line Loop(23) = {3, -11, -7, 12};Surface(24) = {23};
    Surface Loop(1072) = {14, 18, 20, 22, 16, 24};
    Volume(1092) = {1072};
  EndIf
  If(flag_shape==CONE)
    Point(20) = { 0, cone_ry, -cone_h/3, In_lc}; 
    Point(21) = { 0,-cone_ry, -cone_h/3, In_lc};
    Point(22) = { cone_rx, 0, -cone_h/3, In_lc}; 
    Point(23) = {-cone_rx, 0, -cone_h/3, In_lc};
    Point(24) = {0, 0, -cone_h/3, In_lc};
    Point(26) = {0, 0, 2*cone_h/3, In_lc};
    Ellipse(1) = {23, 24, 21, 21};
    Ellipse(2) = {21, 24, 22, 22};
    Ellipse(3) = {22, 24, 20, 20};
    Ellipse(4) = {20, 24, 24, 23};
    Line(5)={20,26};Line(6)={21,26};
    Line(7)={22,26};Line(8)={23,26};
    Line Loop(9) = {1, 2, 3, 4};Plane Surface(10) = {9};
    Line Loop(11) = {6, -8, 1};Surface(12) = {11};
    Line Loop(13) = {8, -5, 4};Surface(14) = {13};
    Line Loop(15) = {5, -7, 3};Surface(16) = {15};
    Line Loop(17) = {7, -6, 2};Surface(18) = {17};
    Surface Loop(1072) = {12, 18, 16, 14, 10};
    Volume(1092) = {1072};
  EndIf
  If (flag_shape==TOR)
    Point(20) = { 0, tor_r1+tor_r2x+tor_r2x ,  0, In_lc};
    Point(21) = { 0, tor_r1+tor_r2x-tor_r2x ,  0, In_lc};
    Point(22) = { 0, tor_r1+tor_r2x ,  tor_r2z, In_lc};
    Point(23) = { 0, tor_r1+tor_r2x , -tor_r2z, In_lc};
    Point(24) = { 0, tor_r1+tor_r2x, 0, In_lc};
    Ellipse(1) = {21, 24, 22, 22};
    Ellipse(2) = {22, 24, 20, 20};
    Ellipse(3) = {20, 24, 23, 23};
    Ellipse(4) = {23, 24, 21, 21};
    Line Loop(5) = {2, 3, 4, 1};Plane Surface(6) = {5};
    Extrude {{0, 0, 1}, {0, 0, 0}, deg2rad*tor_angle/2} {Surface{6};}
    Extrude {{0, 0, 1}, {0, 0, 0},-deg2rad*tor_angle/2} {Surface{6};}
    Surface Loop(1072) = {37, 41, 45, 49, 50, 15, 19, 23, 27, 28};    
  EndIf
  // Rotate {{0, 0, 1}, {0, 0, 0}, rot_phi*deg2rad} {Volume{1092};}
  // Rotate {{-Sin[rot_phi*deg2rad], Cos[rot_phi*deg2rad], 0}, {0, 0, 0}, rot_theta*deg2rad} {Volume{1092};}

  Circle(114) = {2 , 13, 4};
  Circle(115) = {4 , 13, 1};
  Circle(116) = {1 , 13, 3};
  Circle(117) = {3 , 13, 2};
  Circle(118) = {5 , 13, 3};
  Circle(119) = {3 , 13, 6};
  Circle(120) = {6 , 13, 4};
  Circle(121) = {4 , 13, 5};
  Circle(122) = {6 , 13, 2};
  Circle(123) = {2 , 13, 5};
  Circle(124) = {5 , 13, 1};
  Circle(125) = {1 , 13, 6};
  Circle(126) = {8 , 13, 10};
  Circle(127) = {10, 13, 7};
  Circle(128) = {7 , 13, 9};
  Circle(129) = {9 , 13, 8};
  Circle(130) = {8 , 13, 11};
  Circle(131) = {11, 13, 7};
  Circle(132) = {7 , 13, 12};
  Circle(133) = {12, 13, 8};
  Circle(134) = {11, 13, 10};
  Circle(135) = {10, 13, 12};
  Circle(136) = {12, 13, 9};
  Circle(137) = {9 , 13, 11};

  Line Loop(1054) = {123, 118, 117};Surface(1055) = {1054};
  Line Loop(1056) = {118, -116, -124};Surface(1057) = {1056};
  Line Loop(1058) = {116, 119, -125};Surface(1059) = {1058};
  Line Loop(1061) = {119, 122, -117};Surface(1062) = {1061};
  Line Loop(1063) = {114, -120, 122};Surface(1064) = {1063};
  Line Loop(1065) = {114, 121, -123};Surface(1066) = {1065};
  Line Loop(1067) = {121, 124, -115};Surface(1068) = {1067};
  Line Loop(1069) = {125, 120, 115};Surface(1070) = {1069};

  Line Loop(1074) = {128, 137, 131}; Surface(1075) = {1074};
  Line Loop(1076) = {129, 130, -137}; Surface(1077) = {1076};
  Line Loop(1078) = {136, 129, -133}; Surface(1079) = {1078};
  Line Loop(1080) = {128, -136, -132}; Surface(1081) = {1080};
  Line Loop(1082) = {134, 127, -131}; Surface(1083) = {1082};
  Line Loop(1084) = {127, 132, -135}; Surface(1085) = {1084};
  Line Loop(1086) = {135, 133, 126}; Surface(1087) = {1086};
  Line Loop(1088) = {126, -134, -130}; Surface(1089) = {1088};
  // pml in
  Surface Loop(1100) = {1057, 1055, 1066, 1064, 1070, 1059, 1062, 1068};
  // pml out
  Surface Loop(1101) = {1089, 1087, 1085, 1083, 1075, 1081, 1079, 1077};

  
  Surface Loop(1071) = {1062, 1059, 1057, 1068, 1066, 1064, 1070, 1055};
  Volume(1073) = {1071, 1072};
  Surface Loop(1090) = {1075, 1081, 1079, 1077, 1089, 1087, 1085, 1083};
  Volume(1091) = {1071, 1090};
  If (flag_shape==TOR)
    Physical Volume(1) = {1,2}; // Scatterer
  Else
    Physical Volume(1) = {1092}; // Scatterer
  EndIf
  Physical Volume(2) = {1073}; // Out - air
  Physical Volume(3) = {1091}; // pml
  Physical Point(2000) = {1}; // PrintPoint
EndIf

// Mesh.Algorithm   = 1; // // 1=MeshAdapt, 5=Delaunay, 6=Frontal
// Mesh.Algorithm3D = 1;//4; // // 1=Delaunay, 4=Frontal
Mesh.Optimize = 1;
Solver.AutoMesh=2;
Geometry.Points = 1;
// Mesh.SurfaceEdges = 0;
Mesh.VolumeEdges = 0;

Printf(Sprintf("nm          = %.9e;",nm)) > "scattering_tmp.py";
Printf(Sprintf("lambda0     = %.9e;",lambda0)) >> "scattering_tmp.py";
Printf(Sprintf("rbb         = %.9e;",rbb)) >> "scattering_tmp.py";
Printf(Sprintf("epsr_In_re  = %.9e;",epsr_In_re)) >> "scattering_tmp.py";
Printf(Sprintf("epsr_In_im  = %.9e;",epsr_In_im)) >> "scattering_tmp.py";
Printf(Sprintf("epsr_Out_re = %.9e;",epsr_Out_re)) >> "scattering_tmp.py";
Printf(Sprintf("epsr_Out_im = %.9e;",epsr_Out_im)) >> "scattering_tmp.py";
Printf(Sprintf("sph_scan    = %.9e;",sph_scan)) >> "scattering_tmp.py";
Printf(Sprintf("r_sph_min   = %.9e;",r_sph_min)) >> "scattering_tmp.py";
Printf(Sprintf("r_sph_max   = %.9e;",r_sph_max)) >> "scattering_tmp.py";
Printf(Sprintf("nb_cuts     = %g;" ,nb_cuts)) >> "scattering_tmp.py";
Printf(Sprintf("npts_theta  = %g;" ,npts_theta)) >> "scattering_tmp.py";
Printf(Sprintf("npts_phi    = %g;" ,npts_phi)) >> "scattering_tmp.py";
Printf(Sprintf("flag_study  = %g;" ,flag_study)) >> "scattering_tmp.py";
Printf(Sprintf("n_max       = %g;" ,n_max)) >> "scattering_tmp.py";
Printf(Sprintf("p_max       = %g;" ,p_max)) >> "scattering_tmp.py";
Printf(Sprintf("siwt        = %g;" ,siwt)) >> "scattering_tmp.py";

