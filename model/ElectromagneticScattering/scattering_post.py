"""
///////////////////////////////
// Author : Guillaume Demesy //
// scattering_post.py        //
///////////////////////////////
"""

Tmatrix_rfull_ab  = np.zeros((2*p_max,2*p_max,nb_cuts),dtype=complex)
Tmatrix_rfull_fy  = np.zeros((2*p_max,2*p_max,nb_cuts),dtype=complex)
Tmatrix_rfull_fz  = np.zeros((2*p_max,2*p_max,nb_cuts),dtype=complex)
tab_E_NTF_t_G = np.zeros((npts_theta,npts_phi,3),dtype=complex)
tab_E_NTF_p_G = np.zeros((npts_theta,npts_phi,3),dtype=complex)
tab_E_NTF_t_PW = np.zeros((npts_theta,npts_phi),dtype=complex)
tab_E_NTF_p_PW = np.zeros((npts_theta,npts_phi),dtype=complex)

my_dir='./run_results/'
if flag_study==0:
    nbNM = 1
    p_max_in=1
elif flag_study==1:
    nbNM = 2
    p_max_in=p_max
elif flag_study==2:
    nbNM = 1
    p_max_in=p_max
    nb_cuts = 3
elif flag_study==3:
    p_max_in=0

if siwt==1:
    outgoing_sph_hankel = hankel2
else:
    outgoing_sph_hankel = hankel1

print('Postprocessing...')
for ke in range(p_max_in):
    for isN in range(nbNM):
        pe = ps[ke]
        ne =int(np.sqrt(pe))
        me = ne*(ne+1) - int(pe)
        aM_nm   = np.zeros((nb_cuts,p_max),dtype=complex)
        bN_nm   = np.zeros((nb_cuts,p_max),dtype=complex)
        fenm_Y  = np.zeros((nb_cuts,p_max),dtype=complex)
        fenm_Z  = np.zeros((nb_cuts,p_max),dtype=complex)
        fhnm_X  = np.zeros((nb_cuts,p_max),dtype=complex)
        for nr in range(nb_cuts):
            r_sph      = r_sphs[nr]
            # print('======>> postprocessing r_sph',r_sph)
            par_gmsh_getdp=open('parameters_gmsh_getdp.dat', 'a')
            par_gmsh_getdp.write('r_sph = %3.15e;\n'  %(r_sph) )
            par_gmsh_getdp.close()
            if flag_study==1:            
                if isN==0:post_filename = my_dir+'E_scat_onsphere_sph_M%g_r%g.dat'%((pe,nr))
                else:     post_filename = my_dir+'E_scat_onsphere_sph_N%g_r%g.dat'%((pe,nr))
            elif flag_study==0:
                post_filename = my_dir+'E_scat_onsphere_sph_PW_r%g.dat'%(nr)
            elif flag_study==2:
                post_filename = my_dir+'E_tot_onsphere_sph_G%g.dat'%(nr)
            res=field_VSH_expansion(post_filename)
            fhnm_X[nr,:]    = res[0]
            fenm_Y[nr,:]    = res[1]
            fenm_Z[nr,:]    = res[2]
            aM_nm[nr,:]     = res[3]
            bN_nm[nr,:]     = res[4]
            FF_Xnm_t        = res[5]
            FF_Xnm_p        = res[6]
            FF_erCrossXnm_t = res[7]
            FF_erCrossXnm_p = res[8]
        if flag_study==1:
            if isN==0:
                Tmatrix_rfull_ab[ke,0:p_max,:]      =  aM_nm.transpose()
                Tmatrix_rfull_ab[ke,p_max:2*p_max,:] =  bN_nm.transpose()
                Tmatrix_rfull_fy[ke,0:p_max,:]      = fhnm_X.transpose()
                Tmatrix_rfull_fy[ke,p_max:2*p_max,:] = fenm_Y.transpose()
                Tmatrix_rfull_fz[ke,0:p_max,:]      = fhnm_X.transpose()
                Tmatrix_rfull_fz[ke,p_max:2*p_max,:] = fenm_Z.transpose()
            if isN==1:
                Tmatrix_rfull_ab[p_max+ke,0:p_max,:]      =  aM_nm.transpose()
                Tmatrix_rfull_ab[p_max+ke,p_max:2*p_max,:] =  bN_nm.transpose()
                Tmatrix_rfull_fy[p_max+ke,0:p_max,:]      = fhnm_X.transpose()
                Tmatrix_rfull_fy[p_max+ke,p_max:2*p_max,:] = fenm_Y.transpose()
                Tmatrix_rfull_fz[p_max+ke,0:p_max,:]      = fhnm_X.transpose()
                Tmatrix_rfull_fz[p_max+ke,p_max:2*p_max,:] = fenm_Z.transpose()
            Tmatrix_ab     = np.mean(Tmatrix_rfull_ab,axis=2)
            Tmatrix_fy     = np.mean(Tmatrix_rfull_fy,axis=2)
            Tmatrix_fz     = np.mean(Tmatrix_rfull_fz,axis=2)
            std_Tmatrix_ab = np.std(Tmatrix_rfull_ab,axis=2)
            std_Tmatrix_fy = np.std(Tmatrix_rfull_fy,axis=2)
            std_Tmatrix_fz = np.std(Tmatrix_rfull_fz,axis=2)
            np.savez('T_matrix_ellips_sphPML_epsre_%.2lf_eps_im_%.2lf.npz'%(epsr_In.real,epsr_In.imag), \
                    Tmatrix_rfull_ab = Tmatrix_rfull_ab, \
                    Tmatrix_rfull_fy = Tmatrix_rfull_fy, \
                    Tmatrix_rfull_fz = Tmatrix_rfull_fz,\
                    r_sphs           = r_sphs,\
                    n_max            = n_max,\
                    ps               = ps,\
                    p_max            = p_max,\
                    lambda0          = lambda0, \
                    Tmatrix_ab       = Tmatrix_ab, \
                    Tmatrix_fy       = Tmatrix_fy, \
                    Tmatrix_fz       = Tmatrix_fz, \
                    std_Tmatrix_ab   = std_Tmatrix_ab,\
                    std_Tmatrix_fy   = std_Tmatrix_fy,\
                    std_Tmatrix_fz   = std_Tmatrix_fz)
        if flag_study==0:
            mean_aM_nm  = np.mean(aM_nm ,axis=0)
            mean_bN_nm  = np.mean(bN_nm ,axis=0)
            mean_fenm_Y = np.mean(fenm_Y,axis=0)
            mean_fenm_Z = np.mean(fenm_Z,axis=0)
            mean_fhnm_X = np.mean(fhnm_X,axis=0)
            std_aM_nm   = np.std( aM_nm ,axis=0)
            std_bN_nm   = np.std( bN_nm ,axis=0)
            std_fenm_Y  = np.std( fenm_Y,axis=0)
            std_fenm_Z  = np.std( fenm_Z,axis=0)
            std_fhnm_X  = np.std( fhnm_X,axis=0)
            for ko in range(p_max):
                tab_E_NTF_t_PW += (1j)**ko * mean_aM_nm[ko]*1j*FF_Xnm_t[:,:,ko] + (1j)**ko * mean_bN_nm[ko]*FF_erCrossXnm_t[:,:,ko]
                tab_E_NTF_p_PW += (1j)**ko * mean_aM_nm[ko]*1j*FF_Xnm_p[:,:,ko] + (1j)**ko * mean_bN_nm[ko]*FF_erCrossXnm_p[:,:,ko]
            I_NTF_PW = (np.abs(tab_E_NTF_t_PW))**2 + (np.abs(tab_E_NTF_p_PW))**2
            plot_farfield(I_NTF_PW[:,:],my_dir+'farfield_PW.pdf')
        if flag_study==2:
            tens_Green_scat=np.zeros((3,3))
            for nr in range(nb_cuts):
                tens_Green_scat[:,nr] = np.loadtxt(my_dir+'E_scat_im_onpt_G%g.dat'%(nr))[8:11]
                for ko in range(p_max):
                    tab_E_NTF_t_G[:,:,nr] += (1j)**ko * aM_nm[nr,ko]*1j*FF_Xnm_t[:,:,ko] + (1j)**ko * bN_nm[nr,ko]*FF_erCrossXnm_t[:,:,ko]
                    tab_E_NTF_p_G[:,:,nr] += (1j)**ko * aM_nm[nr,ko]*1j*FF_Xnm_p[:,:,ko] + (1j)**ko * bN_nm[nr,ko]*FF_erCrossXnm_p[:,:,ko]
            I_NTF_G = (np.abs(tab_E_NTF_t_G))**2 + (np.abs(tab_E_NTF_p_G))**2
            tens_Green_inc = -siwt*np.eye(3)*k_Out/(6.*pi)
            tens_Green_tot = tens_Green_scat+tens_Green_inc
            for k in range(3):plot_farfield(I_NTF_G[:,:,k],my_dir+'farfield_green%g.pdf'%(k))

if flag_study==1:
    print('Tmatrix_ab:')
    print(Tmatrix_ab)
    print('std_Tmatrix_ab:')
    print(std_Tmatrix_ab)
    print('Tmatrix_fy:')
    print(Tmatrix_fy)
    print('np.abs(2*Tmatrix_fy+1):')
    print(np.abs(2*Tmatrix_fy+1))
    print('Tmatrix_fz:')
    print(Tmatrix_fz)
    print('np.abs(2*Tmatrix_fz+1):')
    print(np.abs(2*Tmatrix_fz+1))
    print('np.abs(2*Tmatrix_ab+1):')
    print(np.abs(2*Tmatrix_ab+1))
elif flag_study==0:
    print('anm:')
    print(mean_aM_nm)
    print('bnm')
    print(mean_bN_nm)
elif flag_study==2:
    print('tens_Green_tot')
    print(tens_Green_tot)
    print('tens_Green_tot/G0')
    print(tens_Green_tot/tens_Green_inc[0,0])
    
elif flag_study==3:
    eigs = np.loadtxt(my_dir+'EigenValuesReal.txt',usecols=[5]) + 1j*np.loadtxt(my_dir+'EigenValuesImag.txt',usecols=[5])
    eigs *= (nm/1e-9)**2
    pl.savez(my_dir+'eigs.npz',eigs=eigs)
    pl.figure(figsize=(20,15))
    pl.plot( eigs.real  ,   eigs.imag,'+',mfc='none')
    pl.grid()
    pl.savefig(my_dir+'eigenvalues.pdf')